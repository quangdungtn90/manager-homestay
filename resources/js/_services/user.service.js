import api from "../api";
import {sprintf} from "sprintf-js";
import restApi from "../_helpers/rest_api";

export const userService = {
    login,
    logout,
    getProfile,
};

function login(username,passWord) {
    let uri = api.login;
    let params = {
        account: username,
        password: passWord
    };
    let requestOptions = {
        "path": uri,
        "method": "POST",
        "data": params
    };
    console.log(JSON.stringify(requestOptions));
    return axios.post('/forward-api',requestOptions,{timeout: 60000});

}
function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    // return next({ path: '/' });
}

// Get profile
function getProfile() {
    let uri = api.getDetaillUser;
    let params = {};
    return restApi.get(uri,params);
}
