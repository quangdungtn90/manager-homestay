<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{asset('images/favicon.png')}}" />
    <title>@yield('title')</title>
{{--    <meta name="description" content="@yield('description')">--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" property="base-url" content="{!! url(" /") !!}"/>
    <meta property="og:url"           content="{{\Illuminate\Support\Facades\URL::full()}}" />
    <meta property="og:type"          content="website" />
{{--    <meta property="og:title"         content="@yield('title')" />--}}
{{--    <meta property="og:description"   content="@yield('description')" />--}}
{{--    <meta property="og:image"         content="@yield('image')" />--}}
{{--    <meta property="og:image:width" content="@yield('image-width')" />--}}
{{--    <meta property="og:image:height" content="@yield('image-height')" />--}}

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css?v='.Carbon\Carbon::now()->timestamp) }}">
    <script>
        // console.log = function () {};
    </script>
    <script type="application/javascript">
        var timeNow = '{{ \Carbon\Carbon::now() }}';
    </script>
</head>
