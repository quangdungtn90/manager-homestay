<?php


namespace App\Http\Controllers\Rest;


use App\Connection\ConnectionHelper;

abstract class BaseRest
{
    protected $path;
    protected $data;
    protected $connection;
    protected $token;

    public function __construct(ConnectionHelper $helper, $path, $data, $token)
    {
        $this->connection = $helper;
        $this->path = $path;
        $this->data = $data;
        $this->token = $token;
    }

    abstract function request();

}
