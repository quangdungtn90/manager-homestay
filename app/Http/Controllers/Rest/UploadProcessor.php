<?php


namespace App\Http\Controllers\Rest;


class UploadProcessor extends BaseRest
{

    function request()
    {
        $uri = APP_API . $this->path;
        $token = $this->token;

        $data = json_decode(json_encode($this->data), true);
        $resp = $this->connection->uploadFile($uri, $data,$token);
        return json_encode($resp);
    }
}
