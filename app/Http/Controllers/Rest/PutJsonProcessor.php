<?php


namespace App\Http\Controllers\Rest;


class PutJsonProcessor extends BaseRest
{

    function request()
    {
        $uri = APP_API . $this->path;
        $params = $this->data;
        $token = $this->token;
        $resp = $this->connection->doPut($uri, $params,$token);
        return json_encode($resp);
    }
}
