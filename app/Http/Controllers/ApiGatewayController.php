<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Rest\HttpProcessorManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Response;


class ApiGatewayController extends Controller
{
    public function request(Request $request)
    {
        Log::info('url api:'.APP_API);
        $path = $request->get("path");
        $method = $request->get("method");
        $data = $request->get("data");
        $token = $request->header('accessToken') ? $request->header('accessToken') : '';
        $data = array_map(function ($value){
            return $value === null ? '' : $value;
        },$data);
        $processor = HttpProcessorManager::getInstance()->getProcessor($this->helper, $method, $path, $data,$token);
        if ($processor != null) {
            return $processor->request();
        }
        return null;
    }

    public function uploadImage(Request $request)
    {
        $data = $request->all();

        return response()->json(['success']);

        $url = $this->api.'/api/v1/upload/imageBase64/ffmpeg';
        $headers_res = apache_request_headers();
        if (isset($headers_res['X-FORWARDED-FOR'])){
            $default = $headers_res['X-FORWARDED-FOR'];
        }else{
            $default = '127.0.0.1';
        }
        $client = new Client([
            'headers' =>
                [
                    'Accept-Encoding' => 'gzip',
                    'Content-Type' => 'application/json',
                    'token' => $this->getToken(),
                    'X-FORWARDED-FOR' => $default
                ]
        ]);
        $img = (string) Image::make($data->file)->encode('data-url');

        $body = json_encode([
            'width' => 0,
            'height' => 0,
            'data' => $img,
        ]);

        try {
            $res =$client->post($url,['body'=>$body]);
            if ($res->getStatusCode() == 200){
                $data = json_decode($res->getBody(),true);
                if ($data['code'] == 200){
                    return $data['data'];
                }else{
                    false;
                }
            }
            else{
                return false;
            }
        }catch (RequestException $e){
            return false;
        }
    }

}
