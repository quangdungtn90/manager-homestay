import middlewarePipeline from "./_router/middleware/middlewarePipeline";
require('./bootstrap');
import { createApp } from 'vue';
import Default from "./layouts/Default";
import None from "./layouts/None";
// import VueRouter from 'vue-router'
import { router }  from './_router';
import { store } from  './_store';
import App from "./components/App";
import ElementPlus from 'element-plus';
import localeVI from 'element-plus/lib/locale/lang/vi';
import 'dayjs/locale/vi';

// Vue.component('default-layout',Default);
// Vue.component('none-layout',None);
// Vue.use(VueRouter);
// Vue.use(VueMoment,{
//     moment,
//     localMoment
// });
// Vue.use(moment);
// Vue.use(ElementPlus,{
//     locale: localeVI
// });


router.beforeEach((to,from,next) => {
    if (!to.meta.middleware) {
        return next()
    }else {
        const middleware = to.meta.middleware;
        const context = {
            to,
            from,
            next,
            store
        };


        return middleware[0]({
            ...context,
            next: middlewarePipeline(context, middleware, 1)
        })
    }
});


// const appMain = createApp({
//     el: '#app',
//     router,
//     store,
//     render: h => h(App),
// });

const app = createApp(App)
app.use(store);
app.use(router);
app.component('default-layout',Default);
app.component('none-layout',None);
app.use(ElementPlus,{
    locale: localeVI
});
app.mount('#app')