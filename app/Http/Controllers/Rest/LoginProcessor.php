<?php

namespace App\Http\Controllers\Rest;

use Illuminate\Support\Facades\Log;

class LoginProcessor extends BaseRest
{

    function request()
    {
        $uri = APP_API . $this->path;
        $params = $this->data;
        $token = $this->token;
        $resp = $this->connection->doPost($uri, $params,$token);
//        Log::info($resp);
        return json_encode($resp);
    }
}
