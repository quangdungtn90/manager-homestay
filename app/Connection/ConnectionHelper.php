<?php
/**
 * Created by PhpStorm.
 * User: daua1993
 * Date: 13/10/2017
 * Time: 08:12
 */

namespace App\Connection;

use App\Exceptions\TokenExpireException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Support\Facades\Log;

class ConnectionHelper
{

    /**
     * @var bool Xac dinh xem co in thong tin request ra khong
     */
    private $print = true;
    private $httpRequest;

    public function setRequest($req)
    {
        $this->httpRequest = $req;
    }

    /** Ham GET
     * @param $url
     * @param $params
     * @param bool $add_token
     * @return mixed|null
     */
    public function doGet($url, $params, $token = null)
    {
        $client = new Client();
        $response = null;

        try {
            if ($this->print) {
                $this->printUrl("GET", $url, $params);
            }
            $response = $client->request('GET', $url, [
                'query' => $params,
                'headers' => $this->getDefaultParams($token),
            ]);

        } catch (ClientException $e) {
            Log::info("doGet ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (ServerException $e) {
            Log::info("doGet ServerException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (GuzzleException $e) {
            Log::info("doPost ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (\Exception $e) {
            Log::info("doGet Exception: " . $e->getMessage());
        }

        if ($response != null) {
            $body = $response->getBody();
            if ($body != null) {
                $result = json_decode($body->getContents(), true);
                return $result;
            }
        }
        return null;
    }

    public function doGetBody($url, $params, $add_token = false)
    {
        $client = new Client();
        $response = null;

        try {

            if ($this->print) {
//                $this->printUrl("GET", $url, $params);
                Log::info("GET : " . $url . " data: " . json_encode($params, JSON_PRETTY_PRINT));
            }

            $response = $client->request('GET', $url, [
                'json' => $params,
                'headers' => $this->getDefaultParams(),
            ]);

        } catch (ClientException $e) {
            Log::info("doGet ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (ServerException $e) {
            Log::info("doGet ServerException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (GuzzleException $e) {
            Log::info("doPost ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (\Exception $e) {
            Log::info("doGet Exception: " . $e->getMessage());
        }

        if ($response != null) {
            $body = $response->getBody();
            if ($body != null) {
                $result = json_decode($body->getContents(), true);
                $this->checkLogin($result);
                return $result;
            }
        }
        return null;
    }

    public function doPostQuery($url, $params, $add_token = false, $timeout = 30)
    {
        $client = new Client();
        $response = null;

        try {
//            if ($this->print) {
//                $this->printUrl("POST", $url,"");
//            }

            Log::info("POST " . $url);

            $response = $client->request('POST', $url, [
                'query' => $params,
                'headers' => $this->getDefaultParams(),
            ]);

//            $response = $client->request('GET', $url, [
//                'query' => $params,
//                'headers' => $this->getDefaultParams(),
//            ]);

        } catch (ClientException $e) {
            Log::info("doGet ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (ServerException $e) {
            Log::info("doGet ServerException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (GuzzleException $e) {
            Log::info("doPost ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (\Exception $e) {
            Log::info("doGet Exception: " . $e->getMessage());
        }

        if ($response != null) {
            $body = $response->getBody();
            if ($body != null) {
                $result = json_decode($body->getContents(), true);
                $this->checkLogin($result);
                return $result;
            }
        }
        return null;
    }

    /**Ham POST
     * @param $url
     * @param $params
     * @param bool $add_token
     * @param int $timeout
     * @return mixed|null
     */
    public function doPost($url, $params, $token = null)
    {
        $client = new \GuzzleHttp\Client(['timeout' => 30]);
        $response = null;
//        Log::info("POST : " . $url . " data: " . json_encode($params, JSON_PRETTY_PRINT));
        try {
            $response = $client->request('POST', $url, [
                'json' => $params,
                'headers' => $this->getDefaultParams($token),
            ]);
        } catch (ClientException $e) {
            Log::info("doPost ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (ServerException $e) {
            Log::info("doPost ServerException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (GuzzleException $e) {
            Log::info("doPost ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (\Exception $e) {
            Log::info("doPost Exception: " . $e->getMessage());
        }

        if ($response != null) {
            $body = $response->getBody();
            if ($body != null) {
                $result = json_decode($body->getContents(), true);
//                $this->checkLogin($result);
                return $result;
            }
        }
        return null;
    }

    /** Ham PUT
     * @param $url
     * @param $params
     * @param bool $add_token
     * @return mixed|null
     */
    public function doPut($url, $params, $token = null)
    {
        $client = new \GuzzleHttp\Client(['timeout' => 30]);
        $response = null;

        Log::info("PUT : " . $url . " data: " . json_encode($params, JSON_PRETTY_PRINT));

        try {
            $response = $client->request('PUT', $url, [
                'json' => $params,
                'headers' => $this->getDefaultParams($token),
            ]);
        } catch (ClientException $e) {
            Log::info("doPut ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (ServerException $e) {
            Log::info("doPut ServerException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (\Exception $e) {
            Log::info("doPut Exception: " . $e->getMessage());
        }
        if ($response != null) {
            $body = $response->getBody();
            if ($body != null) {
                $result = json_decode($body->getContents(), true);
                $this->checkLogin($result);
                return $result;
            }
        }
        return null;
    }

    /** Ham DELETE
     * @param $url
     * @param $params
     * @param bool $add_token
     * @return mixed|null
     */
    public function doDelete($url, $params, $add_token = false)
    {
        $client = new \GuzzleHttp\Client();

        Log::info("DELETE : " . $url . " data: " . json_encode($params, JSON_PRETTY_PRINT));

        $response = null;
        try {
//            if ($this->print) {
//                $this->printUrl("DELETE", $url, $params);
//            }
            $response = $client->request('DELETE', $url, [
                'json' => $params,
                'headers' => $this->getDefaultParams(),
            ]);
        } catch (ClientException $e) {
            Log::info("doDelete ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (ServerException $e) {
            Log::info("doDelete ServerException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (\Exception $e) {
            Log::info("doDelete Exception: " . $e->getMessage());
        }
        //dd($response);
        if ($response != null) {
            $body = $response->getBody();
            if ($body != null) {
                $result = json_decode($body->getContents(), true);
                $this->checkLogin($result);
                return $result;
            }
        }
        return null;
    }

    /** Ham get tra ve json object
     * @param $url
     * @param $params
     * @param bool $add_token
     * @return mixed|null
     */
    public function doGetJsonObject($url, $params, $add_token = false)
    {
        $client = new Client();
        $response = null;
        try {
            if ($this->print) {
                $this->printUrl("GET", $url, $params);
            }
            $response = $client->request('GET', $url, [
                'query' => $params,
                'headers' => $this->getDefaultParams(),
            ]);
        } catch (ClientException $e) {
            Log::info("doGetJsonObject ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (ServerException $e) {
            Log::info("doGetJsonObject ServerException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (\Exception $e) {
            Log::info("doGetJsonObject Exception: " . $e->getMessage());
        }
        if ($response != null) {
            $body = $response->getBody();
            if ($body != null) {
                return json_decode($body->getContents());
            }
        }
        return null;
    }

    /** Ham post tra ve json object
     * @param $url
     * @param $params
     * @param bool $add_token
     * @return mixed|null
     */
    public function doPostJsonObject($url, $params, $add_token = false)
    {
        $client = new \GuzzleHttp\Client();
        $response = null;

        Log::info("POST : " . $url . " data: " . json_encode($params, JSON_PRETTY_PRINT));

        try {
            if ($this->print) {
                //$this->printUrl("POST", $url, $params);
            }
            $response = $client->request('POST', $url, [
                'json' => $params,
                'headers' => $this->getDefaultParams(),
            ]);
        } catch (ClientException $e) {
//            dd($e);
            Log::info("doPostJsonObject ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (ServerException $e) {
            Log::info("doPostJsonObject ServerException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (\Exception $e) {
            Log::info("doPostJsonObject Exception: " . $e->getMessage());
        }
        if ($response != null) {
            $body = $response->getBody();
            if ($body != null) {
                return json_decode($body->getContents());
            }
        }
        return null;
    }

    /** Ham lay cac params default de dong di
     * @return array
     */
    private function getDefaultParams($token = null)
    {
        $headers_res = apache_request_headers();
        $defaults = array();
        if ($token){
            $defaults["token"] = $token;
        }else{
            $defaults["token"] = 'Anonymous';
        }
        if (isset($headers_res['X-FORWARDED-FOR'])){
            $defaults['X-FORWARDED-FOR'] = $headers_res['X-FORWARDED-FOR'];
        }else{
            $defaults['X-FORWARDED-FOR'] = '127.0.0.1';
        }

//      Log::info("Token: " . get_user_info("token"));
        return $defaults;
    }

    private function checkLogin($data)
    {
//        dd($data);
        if (!empty($data['code']) && ($data['code'] == 409 || $data['code'] == 401)) {
            session(['isLoggedIn' => false]);
            throw new TokenExpireException();
        }
    }

    /** In ra link cac request
     * @param $method
     * @param $url
     * @param $params
     */
    private function printUrl($method, $url, $params)
    {
        $str = $method . " : " . $url;
        $str .= "?";
        $index = 0;
        foreach ($params as $key => $value) {
            if ($index > 0) {
                $str .= "&";
            }
            $str .= $key . "=" . $value;
            $index++;
        }
        Log::info($str);
    }

    public function uploadFile($uri, $files)
    {
        if (!isset($files) || count($files) == 0) {
            return null;
        }
        Log::info("url: " . $uri);
        $client = new Client();
        $response = null;
        $json = null;
        $dataUpload = array();
        try {
            foreach ($files as $file) {

//                Log::info("path11: " . print_r($file, TRUE));//['path']);
                if (array_key_exists('path', $file)) {
                    $upload = array(
                        'name' => 'file',
                        'contents' => fopen($file['path'], 'r'),
                        'filename' => strtolower($file['name']),
                    );
                    $dataUpload[] = $upload;
                } else {
                    $dataUpload[] = array('name' => $file['name'], 'contents' => $file['contents']);
                }
            }

            $dataUpload[] = array('name' => 'gymId', 'contents' => 1);
            $header = $this->getDefaultParams();
            $response = $client->post($uri, [
                'multipart' => $dataUpload,
                'headers' => $header,
            ]);

            if ($response != null) {
                $body = $response->getBody();
                if ($body != null) {
                    $json = json_decode($body->getContents(), true);
                    if(json_last_error() != JSON_ERROR_NONE){
                        return $body;
                    }
                }
            }
        } catch (\Exception $exception) {
            Log::info("Error: " . $exception);
        }
        return $json;
    }

    public function doPostLogin($url, $params)
    {
        $client = new \GuzzleHttp\Client(['timeout' => 30]);
        $response = null;
//        Log::info("POST : " . $url . " data: " . json_encode($params, JSON_PRETTY_PRINT));
        try {
            $response = $client->request('POST', $url, [
                'json' => $params,
                'headers' => $this->getDefaultParams(),
            ]);
        } catch (ClientException $e) {
            Log::info("doPost ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (ServerException $e) {
            Log::info("doPost ServerException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (GuzzleException $e) {
            Log::info("doPost ClientException: " . $e->getMessage());
            $response = $e->getResponse();
        } catch (\Exception $e) {
            Log::info("doPost Exception: " . $e->getMessage());
        }

        if ($response != null) {
            $body = $response->getBody();
            if ($body != null) {
                $result = json_decode($body->getContents(), true);
//                $this->checkLogin($result);
                return $result;
            }
        }
        return null;
    }

}
