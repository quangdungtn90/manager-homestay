import {userService} from "../_services";
import {router} from "../_router";

const user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null;
const state = user
    ? { status: { loggedIn: true }, user ,profile : null}
    : { status: { loggedIn: false}, user: null };
const  getters = {
};
const actions = {
    login({ dispatch, commit }, { username, password, rememberPassword }) {
        commit('loginRequest', { username });
        let remember = { username: '', password: '' };
        if (rememberPassword){
            remember.username = username;
            remember.password = password;
        }
        localStorage.setItem('remember', JSON.stringify(remember));
        userService.login(username, password)
            .then(
                response => {
                    console.log(response);
                    if (response.data && response.data.status == 200){
                        localStorage.setItem('user', JSON.stringify(response.data));
                        localStorage.setItem('token', response.data.token);
                        console.log(response.data);
                        commit('loginSuccess', response.data);
                        if (response.data.data.role == 1){
                            router.go({name:'UserSystem'})
                        }else{
                            router.go({name:'File'})
                        }
                        // router.go({name:'DashBoard'})
                    }else {
                        commit('loginFailure');
                        dispatch('alert/error', response.data.msg, { root: true });
                    }
                },
            ).catch(function (error) {
                commit('loginFailure', error);
                dispatch('alert/error', 'Hệ thống bận vui lòng thử lại sau', { root: true });
            });
    },
    logout({ commit }) {
        userService.logout();
        commit('logout');
        // router.go({name:'Login'})
        window.location.href = '/';
    },
    getProfile({ commit }) {
        userService.getProfile().then(
            response => {
                console.log(response);
                if (response.data && response.data.code == 200){
                    commit('getProfile',response.data.data)
                }else {

                }
            }
        ).catch(function (error) {
            console.log(error);
            // commit();
        });
    }
};
const mutations = {
    loginRequest(state, user) {
        state.status = { loggingIn: true };
        state.user = user;
    },
    loginSuccess(state, user) {
        state.status = { loggedIn: true };
        state.user = user;
    },
    loginFailure(state) {
        state.status = {};
        state.user = null;
    },
    logout(state) {
        state.status = {};
        state.user = null;
    },
    getProfile(state,data){
        state.profile = data;
    }
};
export const account = {
    namespaced: true,
    state,
    actions,
    getters,
    mutations
};
