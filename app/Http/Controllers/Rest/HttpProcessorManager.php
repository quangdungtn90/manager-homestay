<?php


namespace App\Http\Controllers\Rest;


class HttpProcessorManager
{
    private static $singletonObj = null;

    public static function getInstance()
    {
        if (self::$singletonObj !== null) {
            return self::$singletonObj;
        }

        self::$singletonObj = new self();
        return self::$singletonObj;
    }

    public function getProcessor($conn, $method, $path, $data,$token)
    {
        switch ($method) {
            case "POST":
                return new PostJsonProcessor($conn, $path, $data,$token);
            case "LOGIN":
                return new PostJsonProcessor($conn, $path, $data,$token);
            case "GET":
                return new GetProcessor($conn, $path, $data,$token);
            case 'PUT' :
                return new PutJsonProcessor($conn, $path, $data,$token);
            case 'UPLOAD' :
                return new UploadProcessor($conn, $path, $data,$token);
            default:
                return null;
        }
    }

}
