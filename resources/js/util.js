

export default {
    generateStatus (status){
        let listStatus = [
            {val : 1,text:'Hoạt động'},
            {val : 2,text:'Vô hiệu hóa'},
        ];
        let result;
        listStatus.map((r) =>{
            if (r.val == status){
                return result = r.text;
            }
        });

        return result;
    },
    generateRole (role){
        let listRole = [
            {val : 1,text:'Admin'},
            {val : 2,text:'Khu vực'},
            {val : 3,text:'Đơn vị đo kiểm'},
            {val : 4,text:'Toàn quốc'},
        ];
        let result;
        listRole.map((r) =>{
            if (r.val == role){
                return result = r.text;
            }
        });

        return result;
    },
    generateArea (area){
        let listArea = [
            {val : 1,text:'Miền Bắc'},
            {val : 2,text:'Miền Trung'},
            {val : 3,text:'Miền Nam'},
        ];
        let result;
        listArea.map((r) =>{
            if (r.val == area){
                return result = r.text;
            }
        });

        return result;
    },
    generateNetwork (network){
        let listNetwork = [
            {val : 1,text:'Viettel'},
            {val : 2,text:'Vinaphone'},
            {val : 3,text:'MobiFone'},
            {val : 4,text:'Vietnamobile'},
            {val : 5,text:'GMobile'},
            {val : 6,text:'Đài phát thanh, đài truyền hình'},
        ];
        let result;
        listNetwork.map((r) =>{
            if (r.val == network){
                return result = r.text;
            }
        });

        return result;
    }
}
