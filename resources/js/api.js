export default {
    login: '/auth/login', // Login
    getAllUser: '/auth/user/getAll',
    createUserSystem: '/auth/register',
    getDetaillUser: '/auth/user',
    getUserRole: '/auth/user/role',
    deleteUserSystem: '/auth/delete',
    updateUserSystem: '/auth/update',
    changePassUserSystem: '/auth/change-password',
    getListFile: '/bts/get',
    checkFile: '/bts/check-file',
    deleteFile : '/bts/delete',
    updateFile : '/bts/update'
}
