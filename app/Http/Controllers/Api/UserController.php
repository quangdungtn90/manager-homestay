<?php


namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Carbon;

class UserController extends Controller
{
    public function login(Request $request){
        $credentials = $request->only('account', 'password');
        $token = null;
        if (!($token = JWTAuth::attempt($credentials))) {
            return response()->json([
                'status' => 205,
                'data' => null,
                'msg' => 'Tài khoản hoặc mật khẩu không đúng.'
            ], 200);
        }
        $user = Auth::user();
        if ($user['date_expiration']){
            $date = Carbon::parse($user['date_expiration']);
            $now = Carbon::now();


            if ($date < $now){
//                $user  = User::findOrFail($user['id']);
                $user->update([
                    'status'=>2
                ]);
                return response()->json([
                    'status' => 201,
                    'msg' => 'Tài khoản bị khóa hoặc không còn sử dụng được',
                    'data' => $user,
                ], 200);
            }else{
                $user->update([
                    'status'=>1
                ]);
            }
        }
        return response()->json([
            'status' => 200,
            'msg' => 'Đăng nhập thành công',
            'data' => $user,
            'token' => $token
        ], 200);
    }

    public function register(Request $request){
        $messages = [
            'name.required'    => 'Chưa nhập tên.',
            'email.unique'    => 'Email đã tồn tại. Vui lòng chọn email khác.',
            'email.required' => 'Email là bắt buộc.',
            'phone.required' => 'Số điện thoại là bắt buộc.',
            'email.email'      => 'Không đúng định dạng email.',
            'password.required'      => 'Chưa nhập mật khẩu.',
            'password.min'      => 'Mât khẩu phải chứa ít nhất 6 ký tự.',
            'confirmed.confirmed' => 'Mật khẩu xác nhận không đúng.',
            'role.required' => 'Chưa chọn loại tài khoản.',
            'account.required' => 'Tài khoản đăng nhập là bắt buộc',
            'account.unique' => 'Tài khoản đã tồn tại.'
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|unique:users,email|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role' => 'required',
            'account' => 'required|unique:users,account|max:255',
            'phone' => 'required',
        ], $messages);
        if($validator->fails()){
//            return response()->json($validator->errors()->toJson(), 400);
            $erros_mes = 'Validate register error';
            if ($validator->errors()->has('email')) {
                $erros_mes = $validator->errors()->first('email');
            }
            if ($validator->errors()->has('account')) {
                $erros_mes = $validator->errors()->first('account');
            }
            return response([
                'status' => 204,
                'msg' => $erros_mes,
                'data' => $validator->errors()
            ], 200);
        }
        $user = new User();
        $user->email = $request->get('email');
        $user->name = $request->get('name');
        $user->account = $request->get('account');
        $user->phone = $request->get('phone');
        $user->address = $request->get('address');
        $user->description = $request->get('description');
        $user->region = $request->get('region');
        $user->network_id = $request->get('network_id');
        $user->password = Hash::make($request->get('password'));
        $user->role = (int)$request->get('role');
        $user->date_expiration = $request->get('date_expiration');
        $user->status = 1;
        $user->edit = $request->get('edit');
        $user->save();
        return response([
            'status' => 200,
            'msg' => 'register success',
            'data' => $user
        ], 200);
    }

    public function getInfo(Request $request){
        if ($request->get('id')){
            $user = User::find($request->get('id'));
        }else{
            $user = Auth::user();
        }

        if ($user) {
            return response([
                'status' => 200,
                'msg' => 'Success',
                'data' => $user
            ], 200);
        }

        return response([
            'status' => 400,
            'msg' => 'Không có tài khoản này.',
            'data' => null
        ], 200);
    }

    public function getAll(Request $request){
        $limit = $request->get('limit') ? $request->get('limit') : 20;
        $key = $request->get('search') ? $request->get('search') : '';
        if ($key){
            $user = User::where('account','like','%'.$key.'%')
                ->orWhere('email','like','%'.$key.'%')
                ->orWhere('name','like','%'.$key.'%')
                ->orderBy('id','desc')->paginate($limit);
        }
        else{
            $user = User::orderBy('id','desc')->paginate($limit);
        }
        return response([
            'status' => 200,
            'msg' => 'All user',
            'data' => $user
        ], 200);
    }

    public function gelByRole(Request $request){
        $limit = $request->get('limit') ? $request->get('limit') : 20;
        $role = $request->get('role');
        if ($role){
            $user = User::where('role','=',$role)
                ->orderBy('id','desc')
                ->paginate($limit);
        }else{
            return response([
                'status' => 400,
                'msg' => 'Error',
                'data' => null
            ], 200);
        }

        return response([
            'status' => 200,
            'msg' => 'Success',
            'data' => $user
        ], 200);
    }

    public function update(Request $request){
        $id = $request->id;
        $user = Auth::user();
        if ($user['role'] != 1){
            return response([
                'status' => 201,
                'msg' => 'User không có quyền thực hiện',
                'data' => null
            ], 200);
        }
        $data = $request->all();
        if (!$id){
            return response([
                'status' => 400,
                'msg' => 'error',
                'data' => null
            ], 200);
        }

        if ($request->get('date_expiration')){
            $date = Carbon::parse($request->get('date_expiration'));
            $now = Carbon::now();
            if ($date < $now){
                $data['status'] = 2;
            }else{
                $data['status'] = 1;
            }
        }
        $user  = User::findOrFail($id);
        $user->update($data);

        return response([
            'status' => 200,
            'msg' => 'Success!',
            'data' => $user
        ], 200);
    }

    public function delete(Request $request){
        $id = $request->id;
        $role = Auth::user();
        if ($role['role'] != 1){
            return response([
                'status' => 201,
                'msg' => 'User không có quyền thực hiện',
                'data' => null
            ], 200);
        }
        if (!$id){
            return response([
                'status' => 400,
                'msg' => 'error',
                'data' => null
            ], 200);
        }
        $user  = User::find($id);

        if (!$user){
            return response([
                'status' => 400,
                'msg' => 'error',
                'data' => null
            ], 200);
        }
        if ($user->delete()){
            return response([
                'status' => 200,
                'msg' => 'Success!',
                'data' => null
            ], 200);
        }else{
            return response([
                'status' => 400,
                'msg' => 'error',
                'data' => null
            ], 200);
        }
    }

    public function changePassword(Request $request){
        $id = $request->id;
        if (!$id){
            return response([
                'status' => 400,
                'msg' => 'error',
                'data' => null
            ], 200);
        }
        $user  = User::findOrFail($id);
        $password = Hash::make($request->get('password'));

        $user->update(['password'=>$password]);
        return response([
            'status' => 200,
            'msg' => 'Success!',
            'data' => $user
        ], 200);
    }
}
