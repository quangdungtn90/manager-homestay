export default function auth ({ next, store }){
    if(!store.getters.checkDate){
        return next({
            name: 'Login'
        })
    }
    return next()
}
