import axios from 'axios';
import {authHeader} from './auth-header'

export default {

    DEFAULT_TIME_OUT: 60000,
    /**
     * Send request
     * Method: POST
     *
     * */
    put(url, params) {
        let tm = this.DEFAULT_TIME_OUT;
        console.log("PUT " + url);
        console.log("req = " + JSON.stringify(params));
        let data = {
            "path": url,
            "method": "PUT",
            "data": params
        };
        return this._request(data, tm);
    },
    /**
     * Send request
     * Method: POST
     *
     * */
    post(url, params) {
        let tm = this.DEFAULT_TIME_OUT;
        console.log("POST " + url);
        console.log("req = " + JSON.stringify(params));
        let data = {
            "path": url,
            "method": "POST",
            "data": params
        };
        return this._request(data, tm);
    },

    login(url, params) {
        let tm = this.DEFAULT_TIME_OUT;
        console.log("POST " + url);
        console.log("req = " + JSON.stringify(params));
        let data = {
            "path": url,
            "method": "LOGIN",
            "data": params
        };
        return this._request(data, tm);
    },
    /**
     * Send request
     * Method: GET
     *
     * */
    get(url, params) {
        let tm = this.DEFAULT_TIME_OUT;
        console.log("GET " + url);
        console.log("req = " + JSON.stringify(params));
        let data = {
            "path": url,
            "method": "GET",
            "data": params
        };
        return this._request(data, tm);
    },
    _request(data, tm) {
        let  config = {
            timeout : tm,
            headers : authHeader()
        };
        return axios.post("/forward-api", data, config)

    },
}
