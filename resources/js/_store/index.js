
import { createStore } from 'vuex'

import { account } from './account.module';
import { alert } from './alert.module';



export const store = new createStore({
    modules: {
        account,
        alert,
    }
});
