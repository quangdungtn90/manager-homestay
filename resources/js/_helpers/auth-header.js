export function authHeader() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.token) {
        return { 'accessToken': user.token};
    } else {
        return {};
    }
}
