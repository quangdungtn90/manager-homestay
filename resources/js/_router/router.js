import { createWebHistory, createRouter } from "vue-router";
import VueRouter from "vue-router";
import guest from "./middleware/guest";
import auth from "./middleware/auth";
import Login from "../pages/Login";
import Error from '../pages/404'


const routes = [
    {
        path: '/:pathMatch(.*)*',
        name: 'Error',
        component: Error,
        meta: { layout: 'none' }
    },
    {
        path: '/',
        name: 'Login',
        component: Login,
        meta: {
            layout: 'none',
        },
        middleware: [
            guest
        ],
    },
];


export const router = createRouter({
    history: createWebHistory(),
    routes,

});

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const role = to.meta ? to.meta.role : '';
    let user = JSON.parse(localStorage.getItem('user'));
    const roleUser =  user ? (user.data ? user.data.role: '') : '';

    if (role && role.length) {
        // if (!roleUser) {
        //     // not logged in so redirect to login page with the return url
        //     return next({ path: '/dashboard'});
        // }
        if (roleUser && !role.includes(roleUser)) {
            // role not authorised so redirect to home page

            return next({ path: '/' });
        }
    }

    next();
});