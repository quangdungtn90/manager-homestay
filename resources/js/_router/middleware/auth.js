export default function auth ({ next, store }){
    if(!store.getters.auth && !store.getters.checkDate){
        return next({
            name: 'Login'
        })
    }
    return next()
}
