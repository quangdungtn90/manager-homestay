
<!--End wrapper-->


@if (app()->isLocal())
    <script  type="text/javascript" src="{{ asset('js/app.js?v='.Carbon\Carbon::now()->timestamp) }}"></script>
@else
    <script type="text/javascript" src="{{ asset('js/manifest.js?v='.Carbon\Carbon::now()->timestamp) }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor.js?v='.Carbon\Carbon::now()->timestamp) }}"></script>
    <script type="text/javascript" src="{{ asset('js/app.js?v='.Carbon\Carbon::now()->timestamp) }}"></script>
@endif

</body>

</html>
