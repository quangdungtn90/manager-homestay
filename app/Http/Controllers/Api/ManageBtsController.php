<?php


namespace App\Http\Controllers\Api;

use App\ManageBts;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use ZipArchive;
use Carbon\Carbon;

class ManageBtsController extends Controller
{
    public function create(Request $request){
        $postData = $request->all();
        try {
            $bts = ManageBts::create($postData);
            return response([
                'status' => 200,
                'msg' => 'Success!',
                'data' => $bts
            ], 200);
        }catch (\Exception $e){
            return response([
                'status' => 400,
                'msg' => $e,
                'data' => null
            ], 200);
        }

    }

    public function getList(Request $request){

        $limit = $request->get('limit') ? $request->get('limit') : 20;
        $center_id = $request->get('center_id') ? $request->get('center_id'):'' ;
        $region = $request->get('region') ? $request->get('region'): '';
        $network_id = $request->get('network_id') ? $request->get('network_id'): '';
        $dv = $request->get('dv') ? $request->get('dv') : '';
        $type = $request->get('type') ? $request->get('type') : '';
        $dateFrom = $request->get('dateFrom') ? $request->get('dateFrom') : '';
        $dateTo = $request->get('dateTo') ? $request->get('dateTo') : '';
        $maTram = $request->get('maTram') ? $request->get('maTram') : '';

        if ($type == 1){
            $type  = 'xlsx';
        }elseif ($type == 3){
            $type  = 'jpg';
        }elseif($type==2){
            $type = 'png';
        }
        $user = Auth::user();
        $arr = [];
        array_push($arr,['override','=',null]);
        array_push($arr,['removed','=',null]);
        if ($user['role'] == 3){
            array_push($arr,['user_id','=',$user['id']]);

        }elseif($user['role'] == 2){
            if ($request->get('user_id')){
                array_push($arr,['user_id','=',$request->get('user_id')]);
            }
        }elseif($user['role'] == 4){
            if ($request->get('user_id')){
                array_push($arr,['user_id','=',$request->get('user_id')]);
            }
        }
        else{
            return response([
                'status' => 201,
                'msg' => 'Không có quyền truy cập',
                'data' => null
            ], 200);
        }

        if ($region){
            if ($region !=4){
                array_push($arr,['manage_bts.region','=',$region]);
            }
        }
        if ($network_id){
            array_push($arr,['manage_bts.network_id','=',$network_id]);
        }
        if ($type){
            array_push($arr,['manage_bts.file_type','=',$type]);
        }
        $data = ManageBts::leftJoin('users', 'manage_bts.user_id', '=', 'users.id')
                ->select('manage_bts.*','users.name')
                ->where($arr);

        if ($dateFrom && $dateTo){
            $from = date($dateFrom);
            $to = date($dateTo);
            $data = $data->whereBetween('manage_bts.date_upload',[$from,$to]);
        }else{
            if ($dateFrom){
                $from = date($dateFrom);
                $to = date($dateFrom);
                $data = $data->whereBetween('manage_bts.date_upload',[$from,$to]);
            }elseif ($dateTo){
                $from = date($dateTo);
                $to = date($dateTo);
                $data = $data->whereBetween('manage_bts.date_upload',[$from,$to]);
            }
        }

        if ($maTram){
            $data = $data->where('manage_bts.name', 'like', '%'.$maTram.'%');
        }
        $data = $data
            ->orderBy('manage_bts.date_upload','desc')
            ->orderBy('manage_bts.file_name_old','asc')
            ->paginate($limit);
//        $data = $data->orderBy('id','desc')->paginate($limit);
        return response([
            'status' => 200,
            'msg' => 'Success!',
            'data' => $data
        ], 200);
    }

    public function checkFile(Request $request){
        $fileNames  = $request->get('file_name');
        $networkId = $request->get('network_id');
        $region = $request->get('region');
        $dateUpload = $request->get('date_upload');
        $network = $this->getNetwork($networkId);
        $area = $this->getArea($region);
        $user = Auth::user();
        $user = $user['account'];
        $path = $this->createPath($area,$network,$user,$dateUpload);
        if ($fileNames){
            $arr = explode(',',$fileNames);
            $list = [];
            foreach ($arr as $fileName){
                $filePath = $path.'/'.$fileName;
                $file = ManageBts::where('file_path','like','%'.$filePath.'%')->first();
                if ($file){
                    array_push($list,$file);
                }
            }

            if (sizeof($list)){
                return response([
                    'status' => 200,
                    'msg' => 'Success',
                    'data' => $list
                ], 200);
            }
            else{
                return response([
                    'status' => 201,
                    'msg' => 'No file exits',
                    'data' => null
                ], 200);
            }
        }
        else{
            return response([
                'status' => 400,
                'msg' => 'error check',
                'data' => null
            ], 200);
        }
    }

    public function upload(Request $request){

        if(!$request->hasFile('files')) {
            return response([
                'status' => 201,
                'msg' => 'Chưa chọn file',
                'data' => null
            ], 200);
        }
        $bts_arr = [];
        $user_id = $request->get('user_id');
        $user = $this->getUser($request->get('user_id'));
        $date_upload = $request->get('date_upload');
        $network_id = $request->get('network_id');
        $network = $this->getNetwork($network_id);
        $region = $request->get('region');
        $area = $this->getArea($region);
        $center_id = $request->get('center_id');
        $files = $request->file('files');
        foreach($files as $file)
        {
            if ($file->isValid()){
                $size = $file->getSize();
                $type = $file->getClientOriginalExtension();
                $nameOld = $file->getClientOriginalName();
                $nameOld = explode('.',$nameOld)[0];
                $name = $this->newNameFile($nameOld,$date_upload);
                $path = $this->createPath($area,$network,$user,$date_upload);
                $postData = [
                    'user_id'=>$user_id,
                    'network_id'=>$network_id,
                    'region'=>$region,
                    'file_path'=>$path.'/'.$name.'.'.$type,
                    'file_name'=>$name,
                    'file_name_old'=>$nameOld,
                    'file_type'=>$type,
                    'file_size'=>$this->formatSizeUnits($size),
                    'date_upload'=>$date_upload.' 00:00:00',
                    'center_id'=>$request->file('center_id'),
                    'override' => null
                ];
                $checkFile = ManageBts::where([
                    ['file_path','like','%'.$path.'/'.$name.'.'.$type.'%'],
                ])->first();
                if ($checkFile){
                    ManageBts::where([
                        ['file_path','like','%'.$path.'/'.$name.'.'.$type.'%'],
                    ])->update([
                        'override'=>1,
                        'date_upload'=>$date_upload.' 00:00:00',
                    ]);
                }
                $bts = ManageBts::create($postData);
                array_unshift($bts_arr,$bts);
                $file->move(public_path($path), $name.'.'.$type);
//                $data[] = $name;
            }
        }
        return response([
            'status' => 200,
            'msg' => 'success',
            'data' => $bts_arr
        ], 200);
    }

    public function downloadFile(Request $request){
        $user = Auth::user();

//        if ($user['role'] !=2){
//            return response([
//                'status' => 201,
//                'msg' => 'Tài khoản không có quyền',
//                'data' => null
//            ], 200);
//        }

        $limit = $request->get('limit') ? $request->get('limit') : 20;
        $center_id = $request->get('center_id') ? $request->get('center_id'):'' ;
        $region = $request->get('region') ? $request->get('region'): '';
        $network_id = $request->get('network_id') ? $request->get('network_id'): '';
        $dv = $request->get('dv') ? $request->get('dv') : '';
        $type = $request->get('type') ? $request->get('type') : '';
        $dateFrom = $request->get('dateFrom') ? $request->get('dateFrom') : '';
        $dateTo = $request->get('dateTo') ? $request->get('dateTo') : '';
        $ids = $request->get('list_id');
        $maTram = $request->get('maTram') ? $request->get('maTram') : '';

        if ($ids){
            $listId = explode('_',$ids);

            $data = ManageBts::whereIn('id',$listId);

            $data = $data->orderBy('manage_bts.date_upload','desc')->get()->toArray();

        }
        else{
            if ($type == 1){
                $type  = 'xlsx';
            }elseif ($type == 3){
                $type  = 'jpg';
            }elseif($type==2){
                $type = 'png';
            }

            $arr = [];

            if ($region){
                array_push($arr,['manage_bts.region','=',$region]);
            }
            if ($network_id){
                array_push($arr,['manage_bts.network_id','=',$network_id]);
            }
            if ($type){
                array_push($arr,['manage_bts.file_type','=',$type]);
            }
            $data = ManageBts::leftJoin('users', 'manage_bts.user_id', '=', 'users.id')
                ->select('manage_bts.*','users.name')
                ->where($arr);

            if ($dateFrom && $dateTo){
                $from = date($dateFrom);
                $to = date($dateTo);
                $data = $data->whereBetween('manage_bts.date_upload',[$from,$to]);
            }else{
                if ($dateFrom){
                    $from = date($dateFrom);
                    $to = date($dateFrom);
                    $data = $data->whereBetween('manage_bts.date_upload',[$from,$to]);
                }elseif ($dateTo){
                    $from = date($dateTo);
                    $to = date($dateTo);
                    $data = $data->whereBetween('manage_bts.date_upload',[$from,$to]);
                }
            }
            if ($maTram){
                $data = $data->where('manage_bts.name', 'like', '%'.$maTram.'%');
            }
            $data = $data->orderBy('manage_bts.date_upload','desc')->get()->toArray();

        }
        $fileName = $user['account'].'_'.Carbon::now('Asia/Ho_Chi_Minh')->format('Ymd').'.zip';
        $zip = new ZipArchive;
        if (sizeof($data)){
            if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE | ZipArchive::OVERWRITE == true)
            {
                foreach ($data as $val){
                    $files = public_path($val['file_path']);
                    //$name = Carbon::parse($val['date_upload'])->format('Ymd').'_'.$this->getArea($val['region']).'_'.$this->getNetwork($val['network_id']).'_'.$this->getUser($val['user_id']).'_'.$val['file_name_old'].'.'.$val['file_type'];
                    $name = Carbon::parse($val['date_upload'])->format('Ymd').'_'.$this->getNetwork($val['network_id']).'_'.$this->getUser($val['user_id']).'_'.$val['file_name_old'].'.'.$val['file_type'];
                    $zip->addFile($files,$name);
                }

                $zip->close();
            }
        }

        if (sizeof($data)){
            return response()->download(public_path($fileName))->deleteFileAfterSend(true);
        }
    }

    private function newNameFile($nameOld,$dateUpload){
        $string = trim($nameOld);
        $date = str_replace('-', '',$dateUpload);
        return $string.'_'.$date;
    }

    private function getUser($id){
        $user = User::find($id);
        return $user['account'];
    }

    private function getNetwork($id){
        switch ($id){
            case 1:
                return 'Viettel';
            case 2:
                return 'Vinaphone';
            case 3:
                return 'Mobiphone';
            case 4:
                return 'Vietnamobile';
            case 5:
                return 'GMobile';
            default:
                return '';
        }
    }

    private function getArea($id){
        switch ($id){
            case 1:
                return 'MienBac';
            case 2:
                return 'MienTrung';
            case 3:
                return 'MienNam';
            case 4:
                return 'All';
            default:
                return '';
        }
    }

    private function createPath($area,$network,$client,$date){
        $year = explode('-',$date)[0];
        $path = 'uploads/'.$area.'/'.$network.'/'.$client.'/'.$year;
        return $path;
    }

    private function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function delete(Request $request){
        $id = $request->id;
        $bts = ManageBts::find($id);
        if (!$bts){
            return response([
                'status' => 400,
                'msg' => 'Không tìm thấy File này',
                'data' => null
            ], 201);
        }

        $bts->update(['removed'=>true]);
        return response([
            'status' => 200,
            'msg' => 'Xóa File thành công',
            'data' => $bts
        ], 200);
    }
    public function update(Request $request){
        $id = $request->id;
        $date = $request->get('date');
        $bts = ManageBts::find($id);
        if (!$bts){
            return response([
                'status' => 400,
                'msg' => 'Không tìm thấy File này',
                'data' => null
            ], 201);
        }
        $bts->update(['date_upload'=>$date]);
        return response([
            'status' => 200,
            'msg' => 'Cập nhật thành công',
            'data' => $bts
        ], 200);
    }

}
