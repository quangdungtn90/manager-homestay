<?php


namespace App\Http\Controllers\Rest;


class DeleteJsonProcessor extends BaseRest
{

    function request()
    {

        $this->path = BASE_URL_API . $this->path;
        $params = $this->data;
        $resp = $this->connection->doDelete($this->path, $params);
        return json_encode($resp);

//        $params = $this->data->get("params");
//        $resp = $this->connection->doDelete(BASE_URL_API . $this->path, $params);
//        return json_encode($resp);
    }
}