
export default function guest ({ next, store }){
    if(store.getters.auth){
        let user = JSON.parse(localStorage.getItem('user'));
        if(user){
            if(user.data && user.data.role == 1){
                return next({
                    name: 'UserSystem'
                })
            }else{
                return next({
                    name: 'File'
                })
            }
        }else{
            return next()
        }
        // return next({
        //     name: 'DashBoard'
        // })
    }

    return next()
}
